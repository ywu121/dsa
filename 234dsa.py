#learn some ideas from
#credit https://github.com/pymq/DSA
from random import randrange
from gmpy2 import invert

def generate_keys(g, p, q):
    x = randrange(2, q)  # x < q
    y = pow(g, x, p)
    return x, y

def sign(M, p, q, g, x):
    k = randrange(2, q)  # k < q
    r = pow(pow(g, k, p),1, q)
    s = pow((invert(k, q) * (M + x * r)),1,q)
    return k,r, s


def verify(M, r, s, p, q, g, y):
    w = invert(s, q)
    u1 = pow((M * w),1, q)
    u2 = pow((r * w),1, q)
    v = pow(pow((pow(g, u1, p) * pow(y, u2, p)),1, p),1, q)
    print("w =",w)
    print("u1 =",u1)
    print("u2 =",u2)
    print("v =",v)
    if v == r:
        return True
    return False


if __name__ == "__main__":
    p = 90868198281938911721339557040518862962404373562514847201528390584007721340265122247266048551719436627299256686065762270984152145792630195275021675980948256339467705928935247934180746103120765983551635801124172295139878112727535910123575648806863257183991590143150466585877668615872768556114439265260667605789
    q = 1389422520963977176468465513966425605778676882537
    g = 36994333209390632334793362037832269335511483991229288032480199460360820816975354219856013398742831539257686830662420419290951158740879427557835344569321334367384918278910140364772556883895493393281848479378610812862114411211876850254023664002538523618950278363857508817746760935011714347322674590614966220768
    print("p = ",p)
    print("q = ",q)
    print("g = ",g)
    print("-------------------------------")
    x, y = generate_keys(g, p, q)
    print("public x=",y)
    print("private x=",x)
    print("-------------------------------")
    M = 0x9eee42ec50db4cb95d178bbe88dc3d14b6b531dc
    k,r, s = sign(M, p, q, g, x)
    print("Private k =",k)
    print("Public r =",r)
    print("Public s =",s)
    if verify(M, r, s, p, q, g, y):
        print('Signature verified')
    
